﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ComponentModel.Composition;
using SampleApplication.Services;

namespace SampleApplication.Web.Services
{
	[ServiceBehavior(IncludeExceptionDetailInFaults = true)] //todo: remember to set IncludeExceptionDetailInFaults to false in Production
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]	
	public class SampleService : ISampleService
	{
		[Import]
		private ISampleSentenceService sentenceService;
		
		public string GetHelloWorld()
		{
			string response = null;

			try
			{
				response = sentenceService.GetSentence();
			}
			catch (Exception ex)
			{
				response = "ERROR!";
			}

			return response;
		}		
	}
}