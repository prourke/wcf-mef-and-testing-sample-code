﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using System.ServiceModel.Configuration;

namespace SampleApplication.Web.ServiceModel
{
	public class EndpointComposedElement : BehaviorExtensionElement
	{
		[ConfigurationProperty("assemblies")]
		[ConfigurationCollection(typeof(NameValueConfigurationCollection))]
		public NameValueConfigurationCollection ConfigAssemblies
		{
			get
			{
				return (NameValueConfigurationCollection)this["assemblies"];
			}
		}

		public override Type BehaviorType
		{
			get { return typeof(ComposedEndpointBehavior); }
		}

		protected override object CreateBehavior()
		{
			ComposablePartCatalog catalog = null;

			var configTypes = ConfigAssemblies;

			if (configTypes != null)
			{
				var assCatalogs = new List<AssemblyCatalog>(configTypes.Count);

				assCatalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly())); //first add the current assembly

				foreach (NameValueConfigurationElement nvce in configTypes) //add any additional assemblies from the configuration
				{
					assCatalogs.Add(new AssemblyCatalog(Assembly.Load(nvce.Value)));
				}

				catalog = new AggregateCatalog(assCatalogs);
			}
			else //no configuration, only use the current assembly
			{
				catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
			}

			var container = new CompositionContainer(catalog);

			return new ComposedEndpointBehavior(container);
		}
	}
}