﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Dispatcher;
using System.ComponentModel.Composition.Hosting;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition;

namespace SampleApplication.Web.ServiceModel
{
	/// <summary>
	/// from: http://www.timjroberts.com/2011/08/wcf-services-with-mef/
	/// </summary>
	public class ComposedInstanceProvider : IInstanceProvider
	{
		private static Dictionary<string, ComposedInstanceProvider> providers = new Dictionary<string, ComposedInstanceProvider>();
		
		private readonly CompositionContainer container;

		public Type ServiceType { get; private set; }
			
		private ComposedInstanceProvider(Type serviceType, CompositionContainer container)
		{			
			ServiceType = serviceType;
			this.container = container;
		}

		public static ComposedInstanceProvider CreateProvider(string address, Type serviceType, CompositionContainer container)
		{
			ComposedInstanceProvider provider = null;

			if (providers.ContainsKey(address))
			{
				provider = providers[address];
			}
			else
			{
				provider = new ComposedInstanceProvider(serviceType, container);
				providers.Add(address, provider);
			}
			
			return provider;
		}

		public static ComposedInstanceProvider GetProvider(string address)
		{
			if (providers.ContainsKey(address))
			{
				return providers[address];
			}
			
			return null;
		}

		public object GetInstance(InstanceContext instanceContext, Message message)
		{			
			return GetInstance(instanceContext);			
		}

		public object GetInstance(InstanceContext instanceContext)
		{			
			Export export = GetServiceExport();

			if (export == null)
			{				
				throw new InvalidOperationException();
			}
		
			return export.Value;
		}

		public void ReleaseInstance(InstanceContext instanceContext, object instance)
		{
			throw new NotImplementedException();
		}

		private Export GetServiceExport()
		{
			var importDefinition
			  = new ContractBasedImportDefinition(AttributedModelServices.GetContractName(ServiceType),
												AttributedModelServices.GetTypeIdentity(ServiceType),
												null,
												ImportCardinality.ZeroOrMore,
												true,
												true,
												CreationPolicy.Any);
			
			return container.GetExports(importDefinition).FirstOrDefault();
		}
	}
}