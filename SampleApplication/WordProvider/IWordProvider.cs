﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace SampleApplication.WordProvider
{
	[InheritedExport]
	public interface IWordProvider
	{
		IEnumerable<string> GetWords();
	}
}
