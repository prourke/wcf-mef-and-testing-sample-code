﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace SampleApplication.WordProvider
{
	public class TextFileWordProvider : IWordProvider
	{
		private const string TEXT_FILE = "SampleApplication.words.txt";

		#region IWordProvider Members

		public IEnumerable<string> GetWords()
		{
			var words = new List<string>();
			
			using (var s = Assembly.GetExecutingAssembly().GetManifestResourceStream(TEXT_FILE))			
			{
				if (s == null) throw new ArgumentException("Couldnt find text file");

				using (var sr = new StreamReader(s))
				{
					while (!sr.EndOfStream)
					{
						words.Add(sr.ReadLine());
					}						
				}				
			}

			return words;			
		}

		#endregion
	}
}
