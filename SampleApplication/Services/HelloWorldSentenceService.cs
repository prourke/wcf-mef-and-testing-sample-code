﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using SampleApplication.WordConcatenator;
using SampleApplication.WordProvider;

namespace SampleApplication.Services
{
	public class HelloWorldSentenceService : ISampleSentenceService
	{
		[Import]
		private IWordProvider wordProvier;

		[Import("spaceConcatenator")]
		private IWordConcatenator wordConcatenator;
		
		#region ISampleSentenceService Members

		public string GetSentence()
		{
			IEnumerable<string> words = wordProvier.GetWords();

			string sentence = wordConcatenator.Concatenate(words);

			return sentence;
		}

		#endregion
	}
}
