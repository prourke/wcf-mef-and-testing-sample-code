﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace SampleApplication.WordConcatenator
{
	[Export("spaceConcatenator", typeof(IWordConcatenator))]
	public class SpaceWordConcatenator : IWordConcatenator
	{
		private const string SEPARATOR = " ";

		#region IWordConcatenator Members

		public string Concatenate(IEnumerable<string> words)
		{
			return String.Join(SEPARATOR, words);
		}

		#endregion
	}
}
