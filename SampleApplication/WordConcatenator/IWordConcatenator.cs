﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace SampleApplication.WordConcatenator
{	
	public interface IWordConcatenator
	{
		string Concatenate(IEnumerable<string> words);
	}
}
