﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SampleApplication.Test.Base.Configuration
{
	public class TestConfigurationSection : ConfigurationSection
	{
		[ConfigurationProperty("Assemblies", IsRequired = true)] 
		public NameValueConfigurationCollection Assemblies
		{
			get
			{
				return this["Assemblies"] as NameValueConfigurationCollection;
			}
		}

	}
}
