﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition.Primitives;
using Moq;

namespace SampleApplication.Test.Base.MockExport
{
	//adapted from http://pastie.org/467842

	public class MockExportDefinition : ExportDefinition
	{
		public object Instance { get; private set; }

		public MockExportDefinition(Type contract, object instance)
			: this(contract, instance, new Dictionary<string, object>())
		{			
		}

		public MockExportDefinition(Type contract, object instance, IDictionary<string, object> metadata)
			: this(contract.FullName, instance, metadata)
		{		
		}

		public MockExportDefinition(string contractName, object instance, IDictionary<string, object> metadata)
			: base(contractName, metadata)
		{
			Instance = instance;
		}		
	}

	public class MockExportDefinition<TContract> : MockExportDefinition
	{
		public MockExportDefinition(TContract instance)
			: this(instance, new Dictionary<string, object>())
		{}

		public MockExportDefinition(TContract instance, IDictionary<string, object> metadata)
			: base(typeof(TContract), instance, metadata)
		{}

		public MockExportDefinition(string contractName, TContract instance)
			: base(contractName, instance, new Dictionary<string, object>())
		{}
	}
}
