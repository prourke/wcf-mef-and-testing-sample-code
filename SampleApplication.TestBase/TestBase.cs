﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Configuration;
using SampleApplication.Test.Base.Configuration;
using SampleApplication.Test.Base.MockExport;

namespace SampleApplication.Test.Base
{
	public abstract class TestBase
	{
		protected void AutoWire() //find assemblies in configuration
		{
			IEnumerable<Assembly> assemblies = GetConfiguredAssemblies();

			AutoWire(null, assemblies.ToArray());
		}

		protected void AutoWire(MockExportProvider mocksProvider) //find assemblies in configuration
		{
			IEnumerable<Assembly> assemblies = GetConfiguredAssemblies();

			AutoWire(mocksProvider, assemblies.ToArray());
		}

		protected void AutoWire(Assembly assembly)
		{
			AutoWire(null, assembly);
		}

		protected void AutoWire(MockExportProvider mocksProvider, params Assembly[] assemblies)
		{
			CompositionContainer container = null;

			var assCatalogs = new List<AssemblyCatalog>();

			foreach (var a in assemblies)
			{
				assCatalogs.Add(new AssemblyCatalog(a));
			}

			if (mocksProvider != null)
			{
				var providers = new List<ExportProvider>();

				providers.Add(mocksProvider); //need to use the mocks provider before the assembly ones			

				foreach (var ac in assCatalogs)
				{
					var assemblyProvider = new CatalogExportProvider(ac);
					providers.Add(assemblyProvider);
				}

				container = new CompositionContainer(providers.ToArray());

				foreach (var p in providers) //must set the source provider for CatalogExportProvider back to the container (kinda stupid but apparently no way around this)
				{
					if (p is CatalogExportProvider)
					{
						((CatalogExportProvider)p).SourceProvider = container;
					}
				}
			}
			else
			{
				container = new CompositionContainer(new AggregateCatalog(assCatalogs));
			}

			container.ComposeParts(this);
		}

		private IEnumerable<Assembly> GetConfiguredAssemblies()
		{
			IEnumerable<Assembly> assemblies = null;

			var config = ConfigurationManager.GetSection("testConfiguration") as TestConfigurationSection;

			if (config != null)
			{
				var assembliesConfig = config.Assemblies;

				assemblies = GetConfiguredAssemblies(assembliesConfig);
			}
			else
			{
				throw new ConfigurationErrorsException("Couldnt find 'testConfiguration' section in configuration file");
			}

			return assemblies;
		}

		private IEnumerable<Assembly> GetConfiguredAssemblies(NameValueConfigurationCollection assembliesConfig)
		{
			List<Assembly> assemblies = null;

			if (assembliesConfig.Count > 0)
			{
				assemblies = new List<Assembly>(assembliesConfig.Count);

				foreach (NameValueConfigurationElement assConf in assembliesConfig)
				{
					assemblies.Add(Assembly.Load(assConf.Value));
				}
			}
			else
			{
				throw new ConfigurationErrorsException("Configuration should include at least one assembly name");
			}

			return assemblies;
		}
	}
}
