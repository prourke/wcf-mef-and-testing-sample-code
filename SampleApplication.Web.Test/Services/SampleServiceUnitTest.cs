﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Test.Base;
using System.ComponentModel.Composition;
using SampleApplication.Web.Services;
using Moq;
using SampleApplication.Services;
using SampleApplication.Test.Base.MockExport;

namespace SampleApplication.Web.Test.Services
{
	[TestClass]
	public class SampleServiceUnitTest : MockTestBase
	{
		[Import]
		private ISampleService service;

		private Mock<ISampleSentenceService> sentenceService = new Mock<ISampleSentenceService>();

		public SampleServiceUnitTest()
		{
			var provider = new MockExportProvider
			(
				GetExport<ISampleSentenceService>(sentenceService)
			);

			base.TestSetUp(provider);
		}

		[TestMethod]
		public void TestGetHelloWorldSuccess()
		{
			string expectedResponse = "hello world";

			sentenceService.Setup(sss => sss.GetSentence()).Returns(expectedResponse);

			string response = service.GetHelloWorld();

			Assert.IsNotNull(response);
			Assert.AreEqual(expectedResponse, response);
		}

		[TestMethod]
		public void TestGetHelloWorldError()
		{
			string expectedResponse = "ERROR!";

			sentenceService.Setup(sss => sss.GetSentence()).Throws(new ArgumentException());

			string response = service.GetHelloWorld();

			Assert.IsNotNull(response);
			Assert.AreEqual(expectedResponse, response);
		}
	}
}
